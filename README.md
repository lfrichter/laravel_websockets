# Laravel Websockets 🌍
@(Laravel)[webdev, laravel]


#### workspace
[![ubuntu](https://img.shields.io/badge/ubuntu-18.04.4LTS-E95420.svg?logo=ubuntu&logoColor=white&style=for-the-badge)](https://www.ubuntu.com/)                                                                                                                                                                                                                                                                                                                                                                                                                                                                   [![Composer](https://img.shields.io/badge/Composer-1.8.6-885630.svg?logo=composer&logoColor=white&style=for-the-badge)](https://getcomposer.org/)                                                                                                                                                                                                                                     [![git](https://img.shields.io/badge/Git-2.17.1-F05032.svg?logo=git&style=for-the-badge&logoColor=white)](https://git-scm.com/downloads)                                                                                                                                                                                                                                                                                                                                                                                                                                                                         [![php](https://img.shields.io/badge/php-7.2.19-777BB4.svg?logo=php&logoColor=white&style=for-the-badge)](http://php.net/)                                                                                                                                                                                                                                                                                                                                                                                                                                                                   [ ![Laravel](https://img.shields.io/badge/Laravel-7.6.2-E74430.svg?logo=laravel&logoColor=white&style=for-the-badge)](https://laravel.com/)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
[![npm](https://img.shields.io/badge/NPM-5.1.0-CB3837.svg?logo=npm&style=for-the-badge&logoColor=white)](https://www.npmjs.com)                                                                                                                                                                                                                                                                                                                                  [![Node.js](https://img.shields.io/badge/Node-12.4.0-339933.svg?logo=node.js&style=for-the-badge&logoColor=white)](https://nodejs.org/en/)

#### mysql
[![mySql](https://img.shields.io/badge/mysql-5.7.26-4479A1.svg?logo=mysql&logoColor=white&style=for-the-badge) ](https://www.mysql.com/)            

#### nginx
[![nginx](https://img.shields.io/badge/nginx-1.14.0-269539.svg?logo=nginx&logoColor=white&style=for-the-badge)](http://nginx.org/)       

#### php-worker
[![Supervisor](https://img.shields.io/badge/Supervisor-3.3.1-Purple.svg?logo=ubuntu&logoColor=white&style=for-the-badge)](http://supervisord.org)       

![Laravel Sockets| center](https://i.imgur.com/wRvhiuY.png)

## Laravel Websockets Implementation 🛰

### Setting Laravel-Echo

1. install `laravel-echo` and `pusher-js`
2. At `resources\js\bootstrap.js` set:
```javascript
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    // encrypted: true,
    // forceTLS: true,
    wsHost: window.location.hostname,
    wsPort: 6001
});
``` 
3. Run `npm install && npm run dev`

### Developmemnt requirements

**Laradock**

1. [Start websocket in workspace container](https://github.com/laradock/laradock/issues/2002#issuecomment-468884705)
2. Add a new supervisor configuration file to container `php-worker`:
    
    Full path file `php-worker\supervisord.d\websockets.conf`
    ```
    [program:websockets]
    process_name=%(program_name)s_%(process_num)02d
    command=php /var/www/artisan websockets:serve
    autostart=true
    autorestart=true
    numprocs=1
    user=laradock
    redirect_stderr=true
    ```
3. Add `EXPOSE 6001` in `php-worker/Dockerfile`
4. And add to `./docker-compose.yml`
   ```
    services:
       php-worker:
       ports:
       - "6001:6001"
   ```
5. Rebuild `php-worker`
  

### Simple ways to test

1. [Laravel sockets painel](http://localhost/laravel-websockets)
2. `npm run dev`
3. Test send message event
    ``` php
    event(new App\Events\NewMessage("Hello World"))
    ```
4. Event Creator
    - Channel: `home`
    - Event: `App\Events\NewMessage`
    - Body:
    ```json
    {
       "message": "Hello World"
    }
    ```
   
#### Private channel

1. Test send message event
   ``` php
   namespace App;
   \Auth::loginUsingId(2, true);
   event(new \App\Events\NewMessagePvt("Hello World"));
   ```
   
   
   
